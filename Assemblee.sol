pragma solidity >=0.5.0 <0.6.0;

contract Assemblee {

    string private nomAssemblee;
    address[] private administrateurs;
    address private adminRoot;
    mapping(address => Member) private members;
    Decision[] private decisionsToVote;

    struct Decision {
        string description;
        bool isOpen;
        uint votesPour;
        uint votesContre;
        mapping(address => bool) votants;
    }

    struct Member {
        address addressMember;
        uint256 blames;
        bool isAdmin;
        bool isBanned;
    }

    constructor(string memory _nom) public {
        nomAssemblee = _nom;
        adminRoot = msg.sender;
        administrateurs.push(adminRoot);
        members[msg.sender] = Member({addressMember: msg.sender, blames: 0, isAdmin: true, isBanned: false});
    }


    // Modifiers
    // Vérifie si l'utilisateur est bien le Président de l'Assemblée
    modifier rootOnly(){
        require(msg.sender == adminRoot, "Vous n'ètes pas le président de cette Assemblée");
        _;
    }

    // Vérifie si l'utilsateur est un administrateur
    modifier adminOnly(){
        require(members[msg.sender].isAdmin, "Vous n'êtes pas administrateur");
        _;
    }

    // Vérifie que le demandeur est membre et n'est pas banni
    modifier isMemberAnNotBanned(address _memberAddress) {
        require(members[_memberAddress].addressMember == _memberAddress, "Vous devez être membre pour proposer une décision!");
        require(members[_memberAddress].isBanned == false, "Ce membre est banni!");
        _;
    }

    //Fonctions d'administration
    function setAdmin(address _newAdmin) public rootOnly {
        require(estMembre(_newAdmin), "Seuls les membres de l'Assemblée peuvent être nommés administrateurs");
        require(members[_newAdmin].isAdmin == false, "Vous êtes déjà administrateur!");
        members[_newAdmin].isAdmin = true;
        administrateurs.push(_newAdmin);
    }

    function adminDismiss() public view adminOnly {
        require(estMembre(msg.sender), "Seuls les administrateurs peuvent démissionnés de leur poste");
        members[msg.sender].isAdmin == false;
    }

    function closeDecision(uint index) public adminOnly {
        decisionsToVote[index].isOpen = false;
    }

    function blameMember(address _memberAddress) public adminOnly isMemberAnNotBanned(_memberAddress){
        if(++members[_memberAddress].blames == 2){
            members[_memberAddress].isBanned = true;
        } else {
            members[_memberAddress].blames++;
        }
    }

    // Fonctionnement de l'Assemblee
    function rejoindre() public {
        require(members[msg.sender].addressMember != msg.sender, "Vous faites déjà partie de cette Assemblée!");
        members[msg.sender] = Member({addressMember: msg.sender, blames: 0, isAdmin: false, isBanned: false});
    }

    //Vérifie que le demandeur est membre de l'Assemblée
    function estMembre(address _utilisateur) public view returns (bool) {
        if(members[_utilisateur].addressMember == _utilisateur){
            return true;
        }
        return false;
    }


    function proposerDecision(string memory _description) public isMemberAnNotBanned(msg.sender) {
        Decision memory decision = Decision({description:_description,votesPour:0,votesContre:0, isOpen:true});
        decisionsToVote.push(decision);
    }


    function voter(uint _indice, bool _vote) public isMemberAnNotBanned(msg.sender){
        Decision storage decision = decisionsToVote[_indice];
        require(decision.isOpen == true, "Les votes sont clos !");
        require(decision.votants[msg.sender] == false, "Vous avez déjà voter !!");
        if(_vote){
            decision.votesPour++;
        } else {
            decision.votesContre++;
        }
        decision.votants[msg.sender] == true;
    }

    function comptabiliser(uint _indice) public view returns (int256) {
        Decision memory decision = decisionsToVote[_indice];

        return int256(decision.votesPour - decision.votesContre);
    }
}
